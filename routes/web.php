<?php

use Illuminate\Support\Facades\Route;

use App\Services\Twitter;

use App\Notifications\SubscriptionRenewalFailed;

use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



/*Route::get('/', function (Twitter $twitter) {
    dd($twitter);
    return view('welcome');
});
*/

Route::get('/notify', function(){
    $user = App\User::first();
    $user->notify(new SubscriptionRenewalFailed);
    return 'done';
});

Route::get('/session', function(){
    return view('welcome');
});

Route::post('/session', function(Request $request){
    //session(['name' => 'Timce']);
    //return session('name','default value');
    
    //$request->session()->put('temp','something new :P');
    return $request->session()->get('temp');
    
    
    return view('welcome');
});

Route::get('/flash', function(){
    return view('flash');
});

Route::post('/session', function(){
    // flash view form data se zema ovde
    session()->flash('message', 'Project has been created');
    return redirect('/session');
});

Route::resource('projects','ProjectsController');

//dokolku postavime avtorizacija preku middleware-ot vo rutite. project zborceto, posle update, e wildcard-ot,moze da go vidime od route:list
// Route::resource('projects','ProjectsController')->middleware('can:update,project'); 

/*
Route::resource se grizi za da se kreiraat site ruti za Restful kontrolerite, da ne mora da se pisuvaat posebno kako podolu

Route::get('/projects', 'ProjectsController@index');
Route::post('/projects', 'ProjectsController@store');
Route::get('/projects/create', 'ProjectsController@create');
Route::get('/projects/{project}/edit', 'ProjectsController@edit');
Route::patch('/projects/{project}', 'ProjectsController@update');
Route::delete('/projects/{project}' , 'ProjectsController@delete');
Route::get('/projects/{project}/show', 'ProjectsController@show');
*/
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/projects/{project}/tasks', 'ProjectTasksController@store');
Route::patch('/tasks/{task}', 'ProjectTasksController@update');