<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Flash function</title>
</head>
<body>
    <form action="/session" method="post">
        @csrf
        <div>
            <input type="text" placeholder="Title" name="title">
        </div>
        <div>
            <textarea placeholder="Description" name="description"></textarea>
        </div>
        <div>
            <button type="submit">Create</button>
        </div>
    </form>
</body>
</html>