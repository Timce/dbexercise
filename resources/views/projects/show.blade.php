@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">

        <!-- Card Header -->
        <div class="card-header bg-info">
            <h2 class="card-header">{{$project->title}}</h2>
        </div>

        <!-- Card Body -->
        <div class="card-body bg-success">
            <p class="lead">{{$project->description}}</p>

            @if($project->tasks->count())
            <h3>Tasks</h3>
            <div class="">
                @foreach($project->tasks as $task)
                <div>
                    
                <!-- Forma za submit-iranje na izbor na checkbox  -->
                <form class="form-inline" method="POST" action="/tasks/{{$task->id}}">
                        @method('PATCH')
                        @csrf
                        <!-- is-complete klasata ke ja kreiram vo app.blade.php so koristenje na internal css no ne mi raboti, ne go preskrtuva zborot-->
                        <label for="completed" class="checkbox {{$task->completed ? 'is-complete' : ''}}"></label>
                        <input name="completed" type="checkbox" onChange="this.form.submit()" {{$task->completed ? 'checked' : ''}}>
                        <div class="mt-1 ml-1"> {{$task->description}} </div>
                    </form>
                    
                </div>
                
                @endforeach
            </div>
            @endif
        </div>

        <!-- Card Footer -->
        <div class="card-footer" style="display: flex">
            <a class="btn btn-info text-white mr-3" href="/projects">Back</a>
        <form class="inline-form" method="POST" action="/projects/{{$project->id}}">
                @method('DELETE')
                @csrf
                <button type="submit" class="btn btn-danger">Delete</button>
            </form>
        </div>
        
    </div>

    <!-- Forma za Kreiranje na nov task -->
<form class="form mt-3" method="POST" action="/projects/{{$project->id}}/tasks">
    @csrf
    <div class="form-group">
        <label for="description">New Task</label>
        <input class="form-control" type="text" name="description" placeholder="Description" required>
    </div>
    <div class="form-group">
        <button type="submit">Add</button>
    </div>

    @include('errors')
</form>
</div>

@endsection