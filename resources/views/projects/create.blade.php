@extends('layouts.app')

@section('content')
<div class="container">
    <h3> Create new project </h3>
    <form class="form" method="POST" action="/projects">
        @csrf
        <div class="form-group">
            <label for="title">Title</label>
            <input class="form-control" type="text" name="title" id="title" value="{{old('title')}}" required><br>
        </div>
        <div class="form-group">
            <label for="description"> Description </label>
        <textarea class="form-control" id="description" name="description" cols="30" rows="10" required>{{old('description')}}</textarea><br>
        </div>
        <div class="form-group">
            <button class="btn btn-primary" type="submit">Create project</button>
        </div>

        @include('errors')
    </form>
</div>
@endsection