@extends('layouts.app')
@section('content')
<div class="container">
    <h1 class="display-4"> Projects </h1>

    <table class="table table-bordered">
        <thead class="table-dark">
            <tr>
                <th style="font-weight: bold; font-size: 20px;"> Title </th>
                <th style="font-weight: bold; font-size: 20px;"> Description</th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
             @foreach ($projects as $project)
                <tr>
                <td> <a style="font-size:15px" href="/projects/{{$project->id}}"> {{$project->title}} </a></td>
                    <td style="font-size:12px"> {{$project->description}}  </td>
                    <td> <a class="btn btn-success" href="/projects/{{$project->id}}/edit"> Edit </a></td>
                    <td> <form method="POST" action="/projects/{{$project->id}}">
                        @method('DELETE')
                        @csrf
                        <button class="btn btn-danger" type="submit">Delete</button>
                    </form></td>
                </tr>
             @endforeach
        </tbody>
        <tfoot>
            <tr><td> <a class="btn btn-warning" href="/projects/create"> Create new </a></td></tr>
        </tfoot>
    </table>
</div>
@endsection
