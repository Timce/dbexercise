@extends('layouts.app')

@section('content')
    <h3> Edit project </h3>
<form class="form" method="POST" action="/projects/{{ $project->id }}">
    {{ method_field('PATCH') }}    
    {{ csrf_field() }}
        
        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" class="form-control" name="title" id="title" value="{{$project->title}}"><br>
        </div>
        <div class="form-group">
            <label for="description"> Description </label>
            <textarea class="form-control" id="description" name="description" cols="30" rows="10">{{$project->description}}</textarea><br>
        </div>
        <div class="form-group">
            <button class="btn btn-success" type="submit">Edit</button>
        </div>
    </form>

    @include('errors')

    <form method="POST" action="/projects/{{$project->id}}">
        @method('DELETE')
        @csrf
        <button class="btn btn-danger" type="submit">Delete</button>
    </form>
@endsection