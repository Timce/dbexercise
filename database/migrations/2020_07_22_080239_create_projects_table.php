<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('owner_id')->nullable();
            $table->string('title');
            $table->text('description');
            $table->timestamps();

            //referenca za nadvoresniot kluc kon primarniot kluc vo users tabelata
            // onDelete('cascade') ke gi izbrise site proekti koi se kreirani od toj user
            $table->foreign('owner_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
