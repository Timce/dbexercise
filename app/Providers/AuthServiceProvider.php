<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Contracts\Auth\Access\Gate;
use App\User;
class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
        'App\Project' => 'App\Policies\ProjectPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */


     // Tuka ke definirame za koi korisnici da ne vazat polisite, odnosno ovoj kod se izvrsuva najprvo. (superseed). vo boot povikuvame Gate fasada 
    public function boot(Gate $gate)
    {
        $this->registerPolicies();

        //
        /*$gate->before(function($user){
            return $user->id == 1; // ova e staticki primer, obicno toa se pravi dinamiski so na primer isAdmin() metod
            // imase nekoj problem, site drugi korisnici nemaat dozvola da pravat nisto, osven korisnikot so ova ID koj moze da pravi se
        });
        */
    }
}
