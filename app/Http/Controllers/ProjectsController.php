<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\Mail\ProjectCreated;
use App\Events\ProjectCreatedEvent;
class ProjectsController extends Controller
{

    public function __construct(){
       /*
       Auth middleware-ot go stavame vo konstruktorot bidejki za site metodi potrebno e korisnikot da e logiran
         Dokolku treba na guest da mu se dozvoli pristap do nekoja akcija mozeme
         $this->middleware('auth')->except('show','index'); ili bilo koi metodi
         Ili da se postavi auth middleware-ot vo web.php fajlot direktno na rutata do metodot

         Mozeme i da postavime auth middleware samo na odredeni metodi so:
         $this->middleware('auth')->only(['delete','updade']);
        */
        $this->middleware('auth');
    }
    public function index()
    {
       //kod so podobra citlivost posle readibility lekcijata

       $projects = auth()->user()->projects;

       // $projects = Project::where('owner_id', auth()->id())->get(); ova e stariot kod koj si raboti okej
        return view('projects.index', compact('projects'));
    }

    public function create()
    {
        return view('projects.create');
    }

    public function store(){

        //Ovoj kod frla MassAssignmentException. Vo modelot mora da se kreira pole $fillable i da se postavat propertijata koi treba da se popolnat(title i desc vo ovoj slucaj).
        //Samoto toa e zastita od laravel, za da ne moze haker da ni gi menuva klucnite propertija kako(id,mbr,broj na smetka i slicno). 
        
        // Treba da se dodade owner_id atributot otkako ke pocnime da filtrirame proekti za daden korisnik
      
        $attributes = $this->validateProject();

        $attributes['owner_id'] = auth()->id();
        
        $project = Project::create($attributes);
        
        event(new ProjectCreatedEvent($project));
        /*
        Raboti i ova no go promeniv poradi validateProject funkcijata
        $project = Project::create([
            'title' => request('title'),
            'description' => request('description'),
            'owner_id' => auth()->id()
        ]);
            */
            // Gornoto moze uste povekje da se uprosti. toa bi izgledalo vaka
            /*
                Project::create(request(['title','description']));
            */ 
        /*
        Kodot pogore e ekvivalenten na ova vo komentarot

        $project = new Project();
        $project->title = request('title');
        $project->description = request('description');

        $project->save();
        */


        // Instanca od mail koja treba da ja pratime na korisnikot deka e kreiran proekt

        \Mail::to($project->owner->email)->send(
            new ProjectCreated($project)
        );

        return redirect('/projects');
        
    }

    /*
    Poopsta verzija na funkcijata
    public function edit($id){

        $project = Project::find($id);
        return view('projects.edit', compact('project')); 
    }
    */

    //Pocista verzija so Model Binding
    public function edit(Project $project){

        $this->authorize('update', $project);
        return view('projects.edit', compact('project'));
    }

    /*
    Poopsta verzija, raboti i napisana na ovoj nacin
    public function update($id){
        $project = Project::find($id);
        $project->title = request('title');
        $project->description = request('description');
        $project->save();

        return redirect('/projects');
    }
    */

    // Pocista verzija, so Model binding
    public function update(Project $project){

        $this->authorize('update', $project);

        //Ova e dosta skratena verzija. Se koristi i kaj create no tamu samo go iskomentirav, go koristam malku poslozeniot nacin.
        $project->update($this->validateProject());
        /*
        Gorniot kod e ekvivalenten na ova
        $project->title = request('title');
        $project->description = request('description');
        $project->save();
        */
        return redirect('/projects');
    }


    // I ovde iskoristiv model binding
    public function destroy(Project $project){
        $this->authorize('update', $project);
        $project->delete();
        return redirect('/projects');
    }

    // I ovde iskoristiv model binding
    public function show(Project $project)
    {
        //$this->authorize('view', $project); 
        //Bidejki treba istata logika da se primeni na site funkcii vo kontrolerot ke imame samo update metod vo polisata

        // Moze da se iskoristat i

        // abort_if() i abort_unless() no ovie ne se povrzani so policy-a
        // $this->authorize('update', $project);
        /*
            if(\Gate::denies('update',$project))  Gate::allows e isto taka mozno
            {
                abort(403);
            }
        */ 
        // moze da se postavi i preku rutite vo web.php fajlot
        // moze i direkno vo view-to vo show.blade.php so @can('update',$project) <do something..> @endcan

        $this->authorize('update', $project);
        return view('projects.show', compact('project'));
    }


    //Ja kreirame za da ne go pisuvame povekje pati kodot za validacija na proekt
    protected function validateProject(){

        return request()->validate([
            'title' => ['required','min:5'],
            'description' => ['required','min:10'],
        ]);
        
    }
}
