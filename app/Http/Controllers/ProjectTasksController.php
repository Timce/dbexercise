<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use App\Project;
class ProjectTasksController extends Controller
{
    // complete a task
    public function update(Task $task){
        /*
            Stariot kod bez enkapsulacija

            $task->update([
                'completed' => request()->has('completed')
            ]);
        */

        // Enkapsuliran

       // $task->complete(request()->has('completed')); ova rasenie e dobro i se koristi koga imame samo edna funkcija complete() vo model klasata

       // ova resenie e uste podobro i se koristi koga imame 2 funkcii complete() i incomplete() vo model klasata

       request()->has('completed') ? $task->complete() : $task->incomplete();
        return back();
    }

    // Add a task
    public function store(Project $project){
        $attributes = request()->validate(['description' => 'required']);
        $project->addTask($attributes);
        return back();
    }
}
