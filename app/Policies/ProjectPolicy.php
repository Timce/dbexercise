<?php

namespace App\Policies;

use App\Project;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProjectPolicy
{
    use HandlesAuthorization;

    // Kreirani bea site metodi no vo primerot, a i najcesto se koristel samo metodot update. No nemora da znaci zavisi od toa sto ni treba. 
   
    
    public function update(User $user, Project $project)
    {
       return $project->owner_id === $user->id;
    }



}
   /* Go dodadov i view metodot

  //  Bidejki logikata za site metodi vo kontrolerot e ista,odnosno avtoriziran korisnik smee da si gi gleda samo negovite proekti
  //  ke koristime samo eden metod, resivme da go krstime update 
    
    
   // public function view(User $user, Project $project)
   // {
   //     return $project->owner_id == $user->id;
   // }
    

    
   // Se dodava prasalnik na pocetok za da se napravi opcionalno, odnosno da moze i guest korisnicite da imaat pristap
  //  public function view( ?User $user, Project $project)
   // {

   // }
*/


