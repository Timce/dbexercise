<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Task;
class Project extends Model
{
   protected $fillable = [
        'title', 'description', 'owner_id'
    ];

    //protected $guarded = []; e sprotivno na $fillable. Tuka kazuvame koi polinja ne smeat da se menuvaat. Ako e prazno znaci site mozat da se menuvaat i nema da se pojavi
    // MassAssignmentException ako go ostavime prazno. 

    public function tasks(){
       return $this->hasMany(Task::class);
    }

    public function owner(){
       return $this->belongsTo(User::class);
    }

    public function addTask($task){
      $this->tasks()->create($task);

      //Poednostaven nacin za kreiranje na task za proektot e
      // $this->project->tasks()->create(compact('description'));  - Nema potreba da go predavame i id-to na proektot, bidejki laravel go znae asociraniot proekt
      //za taskot koj go kreirame od relacijata pomegu project i task
  }
}
