<?php

namespace App\Events;


use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

// Gi izbrisav importite i metodite koi bea za broadcast i ne ni se potrebni vo momentot.

class ProjectCreatedEvent
{
    use Dispatchable, SerializesModels;

    public $project;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($project)
    {
        //
        $this->project=$project;
    }
}
