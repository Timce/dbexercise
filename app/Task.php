<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    //
    protected $guarded = [];
    public function project(){
        return $this->belongsTo(Project::class);
    }

    // Ako od kontrolerot predadime boolean so vrednost true
    public function complete($completed = true)
    {
        $this->update(compact('completed'));
    }

    // Ako od kontrolerot predadime boolean so vrednost false, ja povikuvame complete() samo so false parametar
    public function incomplete(){
        $this->complete(false);
    }

}
